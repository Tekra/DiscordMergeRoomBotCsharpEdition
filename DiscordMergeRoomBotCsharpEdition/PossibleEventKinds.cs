﻿namespace DiscordMergeRoomBotCsharpEdition
{
    public class PossibleEventKinds
    {
        public string MergeRequest { get; set; }

        public string Note { get; set; }
    }
}
