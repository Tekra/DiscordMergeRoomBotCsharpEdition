﻿using DiscordMergeRoomBotCsharpEdition.Services;
using DiscordMergeRoomBotCsharpEdition.Webhooks;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace DiscordMergeRoomBotCsharpEdition.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RequestsController : ControllerBase
    {
        private readonly DataService _dataService;
        private readonly DiscordBotConfiguration _discordBotConfiguration;
        private readonly IEnumerable<IEventHook> _eventHooks;

        public RequestsController(
            IEnumerable<IEventHook> eventHooks,
            DataService dataService,
            DiscordBotConfiguration discordBotConfiguration)
        {
            _dataService = dataService;
            _discordBotConfiguration = discordBotConfiguration;
            _eventHooks = eventHooks;
        }

        [HttpPost]
        public async Task<IActionResult> ParsePoint([FromBody] JsonElement request)
        {
            var json = request.ToBD();

            json.TryGetValue("object_kind", out var objectKind);

            var objectKindName = objectKind.ToString();

            if (_discordBotConfiguration.PossibleObjectKinds.All(x => x != objectKindName))
            {
                return Ok($"Object kind not found '{objectKind}'");
            }

            var projectId = json["project"]["id"].ToString();
            var guildId = await _dataService.GetGuildId(projectId!);

            if (guildId == null)
            {
                return NotFound("Project not found!");
            }

            await _eventHooks.First(e => e.Name == objectKindName).Parse(json, guildId);

            return Ok("WebHook parsed");
        }
    }
}
