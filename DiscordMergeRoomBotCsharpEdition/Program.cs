using Discord;
using Discord.WebSocket;
using DiscordMergeRoomBotCsharpEdition.Commands;
using DiscordMergeRoomBotCsharpEdition.Services;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition
{
    public class Program
    {
        public static List<ICommandHandler> CommandHandlers { get; set; }

        public static Task Main(string[] args)
        {
            return CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }

        public static async Task InitializeDiscordClient(IServiceProvider services)
        {
            var config = services.GetRequiredService<IConfiguration>();
            var botToken = config["BotToken"];
            var mongoClient = services.GetRequiredService<IMongoClient>();

            var client = services.GetRequiredService<DiscordSocketClient>();

            client.Log += Log;
            client.Ready += async () =>
            {
                Console.WriteLine($"Connected as {client.CurrentUser.Username}#{client.CurrentUser.Discriminator}");

                var guildIdStr = config["GuildId"];
                if (string.IsNullOrEmpty(guildIdStr))
                {
                    throw new ArgumentNullException("GuildId", "GuildId is not set in the configuration.");
                }

                if (!ulong.TryParse(guildIdStr, out var guildId))
                {
                    throw new ArgumentException("GuildId is not a valid ulong.");
                }

                var guild = client.GetGuild(guildId);
                if (guild == null)
                {
                    Console.WriteLine($"Guild with ID {guildId} not found. Make sure the bot is invited to the server.");
                    return;
                }

                Console.WriteLine($"Guild with ID {guildId} found.");

                // Регистрация команд
                var commands = new List<SlashCommandBuilder>
                {
                    new SlashCommandBuilder().WithName("ping").WithDescription("Responds with Pong!"),
                    new SlashCommandBuilder().WithName("help").WithDescription("Shows help message"),
                    new SlashCommandBuilder().WithName("echo").WithDescription("Echoes a message")
                        .AddOption("message", ApplicationCommandOptionType.String, "Message to echo", true),
                    new SlashCommandBuilder().WithName("register_project").WithDescription("Registers a project")
                        .AddOption("link", ApplicationCommandOptionType.String, "Link to the project", true)
                        .AddOption("name", ApplicationCommandOptionType.String, "Name of the category project", true),
                };

                var existingGlobalCommands = await client.GetGlobalApplicationCommandsAsync();

                // Удаление всех существующих глобальных команд
                foreach (var command in existingGlobalCommands)
                {
                    await command.DeleteAsync();
                }

                foreach (var command in commands)
                {
                    await guild.CreateApplicationCommandAsync(command.Build());
                }
            };

            client.InteractionCreated += HandleInteraction;

            // Регистрация командных обработчиков напрямую
            CommandHandlers = new List<ICommandHandler>
            {
                new PingCommandHandler(),
                new HelpCommandHandler(),
                new EchoCommandHandler(),
                new RegisterCommandHandler(client, mongoClient, services.GetRequiredService<GitLabService>()),
            };

            await client.LoginAsync(TokenType.Bot, botToken);
            await client.StartAsync();
        }

        private static Task Log(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }

        private static async Task HandleInteraction(SocketInteraction interaction)
        {
            if (interaction is SocketSlashCommand command)
            {
                var commandHandler = CommandHandlers.FirstOrDefault(h => h.Name == command.Data.Name);
                if (commandHandler != null)
                {
                    await commandHandler.HandleCommand(command);
                }
                else
                {
                    await command.RespondAsync("Command not found.");
                }
            }
        }
    }
}
