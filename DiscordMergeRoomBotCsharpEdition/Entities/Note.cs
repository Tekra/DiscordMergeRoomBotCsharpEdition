﻿using MongoDB.Bson;

namespace DiscordMergeRoomBotCsharpEdition.Entities
{
    /// <summary>
    /// Entity for Gitlab note.
    /// </summary>
    public class Note
    {
        /// <summary>
        /// Gitlab note data.
        /// </summary>
        public BsonDocument Data { get; set; }

        /// <summary>
        /// Gitlab user creator note.
        /// </summary>
        public User Creator { get; set; }

        /// <summary>
        /// Bot Additional description.
        /// </summary>
        public string AdditionalDescription { get; set; }

        /// <summary>
        /// Merge request note.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gitlab note URL.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gitlab project name.
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// Gitlab project URL.
        /// </summary>
        public string ProjectUrl { get; set; }

        /// <summary>
        /// Code changes area.
        /// </summary>
        public string CodeArea { get; set; }
    }
}
