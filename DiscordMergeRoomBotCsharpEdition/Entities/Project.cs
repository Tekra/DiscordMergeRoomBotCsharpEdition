﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiscordMergeRoomBotCsharpEdition.Entities
{
    /// <summary>
    /// Project mongo-entity.
    /// </summary>
    [Table("projects")]
    public class Project
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Discord guild ID.
        /// </summary>
        [BsonElement("guildId")]
        public string GuildId { get; set; }

        /// <summary>
        /// Gitlab project ID.
        /// </summary>
        [BsonElement("projectId")]
        public string ProjectId { get; set; }

        /// <summary>
        /// Gitlab project link.
        /// </summary>
        [BsonElement("gitlabLink")]
        public string GitLabLink { get; set; }

        /// <summary>
        /// Discord project category name.
        /// </summary>
        [BsonElement("categoryDiscordName")]
        public string CategoryDiscordName { get; set; }

        /// <summary>
        /// Discord project category ID.
        /// </summary>
        [BsonElement("categoryDiscordId")]
        public string CategoryDiscordId { get; set; }

        /// <summary>
        /// Discord project channel name.
        /// </summary>
        [BsonElement("channelDiscordName")]
        public string ChannelDiscordName { get; set; }

        /// <summary>
        /// Discord project channel id.
        /// </summary>
        [BsonElement("channelDiscordId")]
        public string ChannelDiscordId { get; set; }
    }
}
