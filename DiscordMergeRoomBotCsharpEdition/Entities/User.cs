﻿namespace DiscordMergeRoomBotCsharpEdition.Entities
{
    /// <summary>
    /// Gitlab entity.
    /// </summary>
    /// <remarks>Get by http to gitlab. Use to connect GitlabUser and DiscordUser</remarks>
    public class User
    {
        /// <summary>
        /// Gitlab name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gitlab avatar URL.
        /// </summary>
        public string AvatarUrl { get; set; }

        /// <summary>
        /// Link to Gitlab profile.
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// Discord Id.
        /// </summary>
        public string DiscordId { get; set; }

        /// <summary>
        /// Gitlab Id.
        /// </summary>
        public string Id { get; set; }
    }
}
