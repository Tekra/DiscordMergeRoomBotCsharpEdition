﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiscordMergeRoomBotCsharpEdition.Entities
{
    [Table("mergeRequests")]
    public class MergeRequest
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Merge request name.
        /// </summary>
        [BsonElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Merge request project Id.
        /// </summary>
        [BsonElement("projectId")]
        public int ProjectId { get; set; }

        /// <summary>
        /// Discord channel Id.
        /// </summary>
        [BsonElement("channelId")]
        public string ChannelId { get; set; }

        /// <summary>
        /// Gitlab author Id.
        /// </summary>
        [BsonElement("authorId")]
        public string AuthorId { get; set; }

        /// <summary>
        /// Gitlab merge request Id.
        /// </summary>
        [BsonElement("gitlabMrId")]
        public string GitlabMrId { get; set; }

        /// <summary>
        /// Merge request close state.
        /// </summary>
        [BsonElement("isClosed")]
        public bool IsClosed { get; set; }
    }
}
