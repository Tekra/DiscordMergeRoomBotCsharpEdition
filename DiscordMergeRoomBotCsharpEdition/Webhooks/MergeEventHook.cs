﻿using Discord;
using DiscordMergeRoomBotCsharpEdition.Entities;
using DiscordMergeRoomBotCsharpEdition.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition.Webhooks
{
    public class MergeEventHook : IEventHook
    {
        private readonly DataService _dataService;
        private readonly IMongoClient _mongoClient;
        private readonly IPusherService _pusherService;

        public MergeEventHook(
            IMongoClient mongoClient,
            DataService dataService,
            IPusherService pusherService)
        {
            _mongoClient = mongoClient;
            _dataService = dataService;
            _pusherService = pusherService;
        }

        public string Name => "merge_request";

        public async Task Parse(BsonDocument body, string guildId)
        {
            var action = body["object_attributes"]["action"].ToString();

            switch (action)
            {
                case "update":
                    if (body["changes"]["title"] != BsonNull.Value)
                    {
                        await EditMergeRequestChat(guildId, body);
                    }
                    else
                    {
                        await PushEventBlockingDiscussionsResolved(guildId, body);
                    }

                    break;
                case "open":
                case "reopen":
                    await OpenMergeRequestChat(guildId, body);
                    break;
                case "merge":
                case "close":
                    await CloseMergeRequestChat(guildId, body);
                    break;
            }
        }

        private async Task OpenMergeRequestChat(string guildId, BsonDocument body)
        {
            try
            {
                await _pusherService.Open(guildId, body);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening merge request chat: {ex}");
            }
        }

        private async Task CloseMergeRequestChat(string guildId, BsonDocument body)
        {
            try
            {
                await _pusherService.Close(guildId, body);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error closing merge request chat: {ex}");
            }
        }

        private async Task EditMergeRequestChat(string guildId, BsonDocument body)
        {
            try
            {
                await _pusherService.Edit(guildId, body);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error editing merge request chat: {ex}");
            }
        }

        private async Task PushEventBlockingDiscussionsResolved(string guildId, BsonDocument body)
        {
            try
            {
                var note = new
                {
                    Data = body,
                    Description = body["object_attributes"]["note"].ToString(),
                    Url = body["object_attributes"]["url"].ToString(),
                    ProjectName = body["project"]["name"].ToString(),
                    ProjectUrl = body["project"]["web_url"].ToString(),
                };

                var embed = new EmbedBuilder()
                    .WithColor(Color.Green)
                    .WithDescription($"## All blocking discussions resolved\n[Note link]({note.Url})")
                    .AddField("Project", $"[{note.ProjectName}]({note.ProjectUrl})", true)
                    .AddField("Merge branch flow", $"{body["object_attributes"]["source_branch"]} -> {body["object_attributes"]["target_branch"]}", true)
                    .WithTimestamp(DateTimeOffset.Now)
                    .Build();

                var guild = _dataService.GetGuild(guildId);
                var database = _mongoClient.GetDatabase("mergeRoomBot");
                var mergeRequests = database.GetCollection<MergeRequest>("mergeRequests");
                var result = await mergeRequests
                    .Find(Builders<MergeRequest>.Filter
                        .Eq("gitlabMrId", body["object_attributes"]["id"].ToString()))
                    .FirstOrDefaultAsync();

                if (result != null)
                {
                    var channel = guild.GetTextChannel(ulong.Parse(result.ChannelId));
                    await channel.SendMessageAsync(embed: embed);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error sending resolved discussions notification: {ex}");
            }
        }
    }
}
