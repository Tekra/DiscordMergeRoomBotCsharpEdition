﻿using MongoDB.Bson;

namespace DiscordMergeRoomBotCsharpEdition.Webhooks
{
    public interface IEventHook
    {
        string Name { get; }

        Task Parse(BsonDocument body, string guildId);
    }
}
