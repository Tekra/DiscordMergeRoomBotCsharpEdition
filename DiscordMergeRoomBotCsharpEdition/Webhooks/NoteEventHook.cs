﻿using Discord;
using DiscordMergeRoomBotCsharpEdition.Entities;
using DiscordMergeRoomBotCsharpEdition.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition.Webhooks
{
    public class NoteEventHook : IEventHook
    {
        private readonly DataService _dataService;
        private readonly GitLabService _gitLabService;
        private readonly IMongoClient _mongoClient;
        private readonly IPusherService _pusherService;

        public NoteEventHook(
            IMongoClient mongoClient,
            GitLabService gitLabService,
            DataService dataService,
            IPusherService pusherService)
        {
            _mongoClient = mongoClient;
            _gitLabService = gitLabService;
            _dataService = dataService;
            _pusherService = pusherService;
        }

        public string Name => "note";

        public async Task Parse(BsonDocument body, string guildId)
        {
            await SendNote(body, guildId);
        }

        private async Task SendNote(BsonDocument body, string guildId)
        {
            try
            {
                var guild = _dataService.GetGuild(guildId);
                var note = new Note
                {
                    Data = body,
                    Creator = await _gitLabService.GetUser(body["user"]["id"].ToString()),
                    AdditionalDescription = string.Empty,
                    Description = body["object_attributes"]["note"].ToString(),
                    Url = body["object_attributes"]["url"].ToString(),
                    ProjectName = body["project"]["name"].ToString(),
                    ProjectUrl = body["project"]["web_url"].ToString(),
                };

                if (body["object_attributes"]["original_position"] != BsonNull.Value)
                {
                    await SetCodeDifference(note, body);
                }

                var embed = GetEmbed(note);

                var mergeRequest = await _mongoClient.GetDatabase("mergeRoomBot")
                    .GetCollection<MergeRequest>("mergeRequests")
                    .Find(Builders<MergeRequest>.Filter.Eq(x => x.GitlabMrId, body["merge_request"]["id"].ToString()))
                    .FirstOrDefaultAsync();

                var channel = guild.GetTextChannel(ulong.Parse(mergeRequest.ChannelId));

                if (!string.IsNullOrEmpty(note.Creator.DiscordId))
                {
                    await channel.AddPermissionOverwriteAsync(
                        guild.GetUser(ulong.Parse(note.Creator.DiscordId)),
                        new OverwritePermissions(viewChannel: PermValue.Allow)
                        );
                }

                await channel.SendMessageAsync(embed: embed);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private Embed GetEmbed(Note note)
        {
            var embedBuilder = new EmbedBuilder()
                .WithColor(new Color(uint.Parse(note.Creator.DiscordId) % 16777215))
                .WithAuthor(note.Creator.Name, note.Creator.AvatarUrl, note.Creator.WebUrl)
                .WithDescription($"{note.AdditionalDescription}{note.Description}\n[Note link]({note.Url})")
                .AddField("Project", $"[{note.ProjectName}]({note.ProjectUrl})", true)
                .AddField("Merge branch flow", $"{note.Data["merge_request"]["source_branch"]} -> {note.Data["merge_request"]["target_branch"]}", true)
                .WithTimestamp(DateTimeOffset.Now);

            return embedBuilder.Build();
        }

        private async Task<string> GetLinesSectionFromCode(string code, int from, int to)
        {
            var countLines = 1;
            var lineSection = "";
            var stringRows = new string[to - from + 1];
            var index = 0;

            for (var i = 0; i < code.Length; i++)
            {
                if (countLines == to + 1)
                {
                    break;
                }

                if (countLines >= from)
                {
                    lineSection += code[i];
                }

                if (code[i] == '\n')
                {
                    countLines++;
                    if (countLines == to + 1)
                    {
                        break;
                    }

                    if (countLines >= from)
                    {
                        stringRows[index++] = lineSection;
                        lineSection = string.Empty;
                    }
                }
            }

            return string.Join("", stringRows).TrimStart();
        }

        private async Task SetCodeDifference(Note note, BsonDocument body)
        {
            note.CodeArea = await _gitLabService.GetRawFileFromBranchByName(
                body["project_id"].ToString(),
                body["object_attributes"]["original_position"]["new_path"].ToString(),
                body["merge_request"]["source_branch"].ToString()
                );

            note.CodeArea = await GetLinesSectionFromCode(
                note.CodeArea,
                body["object_attributes"]["original_position"]["line_range"]["start"]["new_line"].AsInt32,
                body["object_attributes"]["original_position"]["line_range"]["end"]["new_line"].AsInt32
                );

            if (!string.IsNullOrEmpty(note.CodeArea))
            {
                note.AdditionalDescription = $"```fix\n{note.CodeArea}\n```";
            }
        }
    }
}
