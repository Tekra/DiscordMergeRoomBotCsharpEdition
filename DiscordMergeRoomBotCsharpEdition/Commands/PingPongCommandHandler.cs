﻿using Discord.WebSocket;

namespace DiscordMergeRoomBotCsharpEdition.Commands
{
    public class PingCommandHandler : ICommandHandler
    {
        public string Name => "ping";

        public async Task HandleCommand(SocketSlashCommand command)
        {
            if (command.CommandName == Name)
            {
                await command.RespondAsync("Pong!");
            }
        }
    }
}
