﻿using Discord.WebSocket;

namespace DiscordMergeRoomBotCsharpEdition.Commands
{
    public interface ICommandHandler
    {
        string Name { get; }

        Task HandleCommand(SocketSlashCommand command);
    }
}
