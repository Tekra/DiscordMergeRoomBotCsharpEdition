﻿using Discord;
using Discord.WebSocket;
using DiscordMergeRoomBotCsharpEdition.Entities;
using DiscordMergeRoomBotCsharpEdition.Services;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition.Commands
{
    public class RegisterCommandHandler : ICommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly IMongoDatabase _database;
        private readonly GitLabService _gitLabService;

        public RegisterCommandHandler(
            DiscordSocketClient client,
            IMongoClient mongoClient,
            GitLabService gitLabService)
        {
            _client = client;
            _gitLabService = gitLabService;
            _database = mongoClient.GetDatabase("mergeRoomBot");
        }

        public string Name => "register_project";

        public async Task HandleCommand(SocketSlashCommand command)
        {
            var linkOption = command.Data.Options.FirstOrDefault(o => o.Name == "link");
            var nameOption = command.Data.Options.FirstOrDefault(o => o.Name == "name");

            if (linkOption == null || nameOption == null)
            {
                await command.RespondAsync("Missing required options.");
                return;
            }

            var link = linkOption.Value.ToString();
            var categoryName = nameOption.Value + "_mr";

            if (link != null && IsValidUrl(link))
            {
                var projectsCollection = _database.GetCollection<Project>("projects");

                var gitlabProject = await _gitLabService.GetProjectDataByUrl(link);

                var project = await _database
                    .GetCollection<Project>("projects")
                    .Find(Builders<Project>.Filter.Eq(mr => mr.ProjectId, gitlabProject.Id)).FirstOrDefaultAsync();

                if (project != null)
                {
                    await command.RespondAsync($"Project with: {link} already registered.");
                    return;
                }

                var guild = _client.GetGuild((ulong)command.GuildId!);
                if (guild == null)
                {
                    await command.RespondAsync("Guild not found.");
                    return;
                }

                var channel = guild.TextChannels.FirstOrDefault(c => c.Name == (string)nameOption.Value);

                if (channel == null)
                {
                    await command.RespondAsync($"Channel with name: {nameOption.Value} not found.");
                    return;
                }

                var permissionOverwrites = new List<Overwrite>
                {
                    new Overwrite(guild.EveryoneRole.Id, PermissionTarget.Role, new OverwritePermissions(viewChannel: PermValue.Deny)),
                };

                var category = await guild.CreateCategoryChannelAsync(categoryName, properties =>
                {
                    properties.PermissionOverwrites = permissionOverwrites;
                });

                await projectsCollection.InsertOneAsync(new Project
                {
                    GuildId = guild.Id.ToString(),
                    ProjectId = gitlabProject.Id,
                    GitLabLink = gitlabProject.Url,
                    ChannelDiscordName = channel.Name,
                    ChannelDiscordId = channel.Id.ToString(),
                    CategoryDiscordName = categoryName,
                    CategoryDiscordId = category.Id.ToString(),
                });

                await command.RespondAsync($"Link: {link} registered. To '{categoryName}'");
            }
            else
            {
                await command.RespondAsync($"Link: {link} NOT CORRECT!");
            }
        }

        private bool IsValidUrl(string url)
        {
            try
            {
                var uri = new Uri(url);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
