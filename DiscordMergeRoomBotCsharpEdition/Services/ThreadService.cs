﻿using Discord;
using Discord.WebSocket;

public class ThreadService
{
    private readonly DiscordSocketClient _client;

    public ThreadService(DiscordSocketClient client)
    {
        _client = client;
    }

    public async Task CreateThread(SocketGuild guild, string channelName, string threadName)
    {
        var channel = guild.TextChannels.FirstOrDefault(c => c.Name == channelName);
        if (channel == null)
        {
            Console.WriteLine($"Channel \"{channelName}\" not found.");
            return;
        }

        // Создаем тред в указанном канале
        var thread = await channel.CreateThreadAsync(threadName, autoArchiveDuration: ThreadArchiveDuration.OneWeek);

        /*// Получаем всех администраторов
        var admins = guild.Users.Where(user => user.GuildPermissions.Administrator);
        var overwrites = new OverwritePermissions(viewChannel: PermValue.Deny);

        // Отключаем доступ для администраторов
        foreach (var admin in admins)
        {
            await thread.AddPermissionOverwriteAsync(admin, overwrites);
        }

        // Отключаем доступ для всех остальных
        await thread.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(viewChannel: PermValue.Deny));*/

        Console.WriteLine($"Thread \"{threadName}:{thread.Id}\" created in channel \"{channelName}:{channel.Id}\".");
    }

    public async Task AddUserToThreadAsync(SocketGuild guild, string threadName, SocketGuildUser user)
    {
        var thread = guild.ThreadChannels.FirstOrDefault(t => t.Name == threadName);
        if (thread == null)
        {
            Console.WriteLine($"Thread \"{threadName}\" not found.");
            return;
        }

        await thread.AddUserAsync(user);

        /*var overwrite = new OverwritePermissions(viewChannel: PermValue.Allow);
        await thread.AddPermissionOverwriteAsync(user, overwrite);*/

        Console.WriteLine($"User {user.Mention} added to thread \"{threadName}:{thread.Id}\".");
    }
}
