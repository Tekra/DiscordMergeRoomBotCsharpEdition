﻿using Discord;
using DiscordMergeRoomBotCsharpEdition.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition.Services
{
    public class ChannelService : IPusherService
    {
        private readonly IMongoDatabase _database;
        private readonly DataService _dataService;
        private readonly GitLabService _gitLabService;

        public ChannelService(
            IMongoClient mongoClient,
            DataService dataService,
            GitLabService gitLabService)
        {
            _database = mongoClient.GetDatabase("mergeRoomBot");
            _dataService = dataService;
            _gitLabService = gitLabService;
        }

        public async Task Open(string guildId, BsonDocument body)
        {
            var mergeCreator = await _gitLabService.GetUser(body["user"]["id"].ToString()!);

            var collection = _database.GetCollection<Project>("projects");
            var project = await collection
                .Find(Builders<Project>.Filter
                    .Eq("gitlabLink", body["project"]["web_url"].ToString()))
                .FirstOrDefaultAsync();

            var guild = _dataService.GetGuild(guildId);

            var permissionOverwrites = new List<Overwrite>
            {
                new Overwrite(guild.EveryoneRole.Id, PermissionTarget.Role, new OverwritePermissions(viewChannel: PermValue.Deny)),
            };

            if (!string.IsNullOrEmpty(mergeCreator.DiscordId))
            {
                permissionOverwrites.Add(new Overwrite(ulong.Parse(mergeCreator.DiscordId), PermissionTarget.User, new OverwritePermissions(viewChannel: PermValue.Allow)));
            }

            var title = body["object_attributes"]["title"].ToString();

            var channel = await guild.CreateTextChannelAsync(title, properties =>
            {
                properties.CategoryId = ulong.Parse(project.CategoryDiscordId);
                properties.PermissionOverwrites = permissionOverwrites;
            });

            var mrTitle = title.Replace("\"", "\\\"").Replace("'", "\\'");
            var mergeRequests = _database.GetCollection<MergeRequest>("mergeRequests");

            var gitlabMrId = body["object_attributes"]["id"].ToString();
            var result = await mergeRequests.Find(Builders<MergeRequest>.Filter.Eq(mr => mr.GitlabMrId, gitlabMrId)).FirstOrDefaultAsync();

            var mr = new MergeRequest
            {
                Id = result?.Id,
                Name = mrTitle,
                ProjectId = body["project"]["id"].AsInt32,
                ChannelId = channel.Id.ToString(),
                AuthorId = body["user"]["id"].ToString(),
                GitlabMrId = gitlabMrId,
                IsClosed = false,
            };

            var filter = Builders<MergeRequest>.Filter.Eq(mr => mr.Id, result?.Id);

            await mergeRequests.ReplaceOneAsync(filter, mr, new ReplaceOptions { IsUpsert = true });
        }

        public async Task Close(string guildId, BsonDocument body)
        {
            var mergeRequests = _database.GetCollection<MergeRequest>("mergeRequests");
            var result = await mergeRequests
                .Find(Builders<MergeRequest>.Filter
                    .Eq("gitlabMrId", body["object_attributes"]["id"].ToString()))
                .FirstOrDefaultAsync();

            var guild = _dataService.GetGuild(guildId);
            var channel = guild.GetTextChannel(ulong.Parse(result.ChannelId));
            await channel.DeleteAsync();

            var update = Builders<MergeRequest>.Update.Set("isClosed", true);
            await mergeRequests.UpdateOneAsync(Builders<MergeRequest>.Filter.Eq(mr => mr.Id, result.Id), update);
        }

        public async Task Edit(string guildId, BsonDocument body)
        {
            var guild = _dataService.GetGuild(guildId);
            var mergeRequests = _database.GetCollection<MergeRequest>("mergeRequests");
            var result = await mergeRequests
                .Find(Builders<MergeRequest>.Filter
                    .Eq("gitlabMrId", body["object_attributes"]["id"].ToString()))
                .FirstOrDefaultAsync();

            if (result != null)
            {
                var title = body["changes"]["title"]["current"].ToString();
                var channel = guild.GetTextChannel(ulong.Parse(result.ChannelId));
                await channel.ModifyAsync(properties => properties.Name = title);
            }
        }
    }
}
