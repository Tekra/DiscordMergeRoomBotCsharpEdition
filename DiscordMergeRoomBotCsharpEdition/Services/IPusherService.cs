﻿using MongoDB.Bson;

namespace DiscordMergeRoomBotCsharpEdition.Services
{
    public interface IPusherService
    {
        Task Open(string guildId, BsonDocument body);

        Task Close(string guildId, BsonDocument body);

        Task Edit(string guildId, BsonDocument body);
    }
}
