﻿using Discord.WebSocket;
using DiscordMergeRoomBotCsharpEdition.Entities;
using MongoDB.Driver;

namespace DiscordMergeRoomBotCsharpEdition.Services
{
    public class DataService
    {
        private readonly DiscordSocketClient _client;
        private readonly IMongoClient _mongoClient;

        public DataService(IMongoClient mongoClient, DiscordSocketClient client)
        {
            _mongoClient = mongoClient;
            _client = client;
        }

        public async Task<string?> GetGuildId(string projectId)
        {
            try
            {
                var database = _mongoClient.GetDatabase("mergeRoomBot");
                var collection = database.GetCollection<Project>("projects");
                var filter = Builders<Project>.Filter.Eq("projectId", projectId);
                var result = await collection.Find(filter).FirstOrDefaultAsync();

                return result?.GuildId;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error retrieving guild id: {ex}");
                return null;
            }
        }

        public SocketGuild GetGuild(string guildId)
        {
            return _client.GetGuild(ulong.Parse(guildId));
        }
    }
}
