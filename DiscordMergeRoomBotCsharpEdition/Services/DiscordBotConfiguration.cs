﻿using System.Collections.ObjectModel;

namespace DiscordMergeRoomBotCsharpEdition.Services
{
    public class DiscordBotConfiguration
    {

        public DiscordBotConfiguration(ReadOnlyCollection<string> possibleObjectKinds)
        {
            PossibleObjectKinds = possibleObjectKinds;
        }

        public ReadOnlyCollection<string> PossibleObjectKinds { get; set; }
    }
}
