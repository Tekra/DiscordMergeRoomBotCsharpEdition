﻿using DiscordMergeRoomBotCsharpEdition.Entities;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DiscordMergeRoomBotCsharpEdition.Services
{
    public class GitLabService
    {
        private readonly string _accessToken;
        private readonly HttpClient _httpClient;

        public GitLabService(HttpClient httpClient, string accessToken)
        {
            _httpClient = httpClient;
            _accessToken = accessToken;
        }

        public async Task<User> GetUser(string id)
        {
            var requestUrl = $"https://gitlab.com/api/v4/users/{id}?access_token={_accessToken}";
            var response = await _httpClient.GetAsync(requestUrl);
            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(jsonResponse);

            return new User
            {
                Id = json["id"].Value<string>(),
                Name = json["name"].Value<string>(),
                WebUrl = json["web_url"].Value<string>(),
                DiscordId = json["discord"].Value<string>(),
                AvatarUrl = json["avatar_url"].Value<string>(),
            };
        }

        public async Task<string> GetRawFileFromBranchByName(string projectId, string filePath, string branchName)
        {
            var requestUrl = $"https://gitlab.com/api/v4/projects/{projectId}/repository/files/{HttpUtility.UrlEncode(filePath)}/raw/?ref={HttpUtility.UrlEncode(branchName)}&access_token={_accessToken}";
            var response = await _httpClient.GetAsync(requestUrl);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<(string Id, string Url)> GetProjectDataByUrl(string url)
        {
            var pattern = @"https://gitlab.com/(?<namespace>[^/]+)/(?<project>[^/]+)/";

            // Use Regex to match the pattern in the URL
            var match = Regex.Match(url, pattern);

            if (!match.Success)
            {
                Console.WriteLine("The URL format is incorrect or does not contain the namespace and project name.");
            }

            var namespaceName = match.Groups["namespace"].Value;
            var projectName = match.Groups["project"].Value;

            var requestUrl = $"https://gitlab.com/api/v4/projects/{namespaceName}%2F{projectName}?access_token={_accessToken}";
            var response = await _httpClient.GetAsync(requestUrl);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(jsonResponse);

            return (json["id"].Value<string>(), json["web_url"].Value<string>());
        }
    }
}
